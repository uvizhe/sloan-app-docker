To run the container:

Clone `sloan-app` repo:

`$ git clone --depth 1 https://uvizhe@bitbucket.org/uvizhe/sloan-app.git`

Set variables in sloan-app/src/config.js.

Install build dependencies and build the code:

```
$ cd sloan-app
$ npm install && npm run build
```

Build Docker image:

`$ docker build -t sloan-app .`

Run a container:

`$ docker run -p 8080:80 sloan-app`
